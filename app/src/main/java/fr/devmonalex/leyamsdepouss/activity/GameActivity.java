package fr.devmonalex.leyamsdepouss.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

//import fr.afpa.dl211.leyamsdepouss.CustomClass.CheckableImageView;
import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

import fr.devmonalex.leyamsdepouss.CustomClass.CheckableImageView;
import fr.devmonalex.leyamsdepouss.R;
import fr.devmonalex.leyamsdepouss.modele.Grille;
import fr.devmonalex.leyamsdepouss.modele.Joueur;
import fr.devmonalex.leyamsdepouss.modele.Partie;
import fr.devmonalex.leyamsdepouss.modele.YamsManager;

/**
 * Created by dl211 on 07/10/2015.
 */
public class GameActivity extends AppCompatActivity {
    private final static int SCORE_BONUS = 63;
    private final static int BARRE = 66;
    //Elements de la vue
    private CheckableImageView des1, des2, des3, des4, des5;
    private Vector<CheckableImageView> vImgDes = new Vector<CheckableImageView>();
    private TextView txJouEnCours, tx1, tx2, tx3, tx4, tx5, tx6, txBrelan, txCarre,
            txFull, txYams, txChance, txPetite, txGrande, txBonus, txScoreSup, txScoreInf,
            txScoreFinal, txInfo;
    private Button btnLancer, btn1, btn2, btn3, btn4, btn5, btn6, btnBrelan, btnCarre,
            btnFull, btnYams, btnChance, btnPetite, btnGrande, btnBarrer;

    private int turn = 1;
    private Partie p;
    private YamsManager yamsM;
    private Vector<Joueur> vJoueurs;
    private Vector<Button> vButtonScore = new Vector<Button>();
    private Vector<TextView> vTextViewSup = new Vector<TextView>();
    private Vector<TextView> vTextViewInf = new Vector<TextView>();
    private Joueur jouEnCours;
    private Grille grillEnCours;
    private int numJoueurEnCours;
    private int nbJoueur;
    private boolean boolBarre = false;
    private int nbTourJeu =13;
    private int numTour = 1;

    private LayoutInflater inflater;
    private View layout_toast;
    private TextView text_toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);
//------------------Element de la vue
        des1 = (CheckableImageView) findViewById(R.id.ivDes1);
        des2 = (CheckableImageView) findViewById(R.id.ivDes2);
        des3 = (CheckableImageView) findViewById(R.id.ivDes3);
        des4 = (CheckableImageView) findViewById(R.id.ivDes4);
        des5 = (CheckableImageView) findViewById(R.id.ivDes5);

        vImgDes.add(des1);
        vImgDes.add(des2);
        vImgDes.add(des3);
        vImgDes.add(des4);
        vImgDes.add(des5);

        txJouEnCours = (TextView) findViewById(R.id.txtNomJoueurEnCours);
        txInfo = (TextView) findViewById(R.id.txInfo);
        btnLancer = (Button) findViewById(R.id.btnLanceDes);
        btnBarrer = (Button) findViewById(R.id.btnBarrer);

        btn1 = (Button) findViewById(R.id.btnUn);
        btn2 = (Button) findViewById(R.id.btnDeux);
        btn3 = (Button) findViewById(R.id.btnTrois);
        btn4 = (Button) findViewById(R.id.btnQuatre);
        btn5 = (Button) findViewById(R.id.btnCinq);
        btn6 = (Button) findViewById(R.id.btnSix);
        btnBrelan = (Button) findViewById(R.id.btnBrelan);
        btnCarre = (Button) findViewById(R.id.btnCarre);
        btnFull = (Button) findViewById(R.id.btnFull);
        btnPetite = (Button) findViewById(R.id.btnPetite);
        btnGrande = (Button) findViewById(R.id.btnGrande);
        btnYams = (Button) findViewById(R.id.btnYams);
        btnChance = (Button) findViewById(R.id.btnChance);

        vButtonScore.add(btn1);
        vButtonScore.add(btn2);
        vButtonScore.add(btn3);
        vButtonScore.add(btn4);
        vButtonScore.add(btn5);
        vButtonScore.add(btn6);
        vButtonScore.add(btnBrelan);
        vButtonScore.add(btnCarre);
        vButtonScore.add(btnFull);
        vButtonScore.add(btnPetite);
        vButtonScore.add(btnGrande);
        vButtonScore.add(btnYams);
        vButtonScore.add(btnChance);


        tx1 = (TextView) findViewById(R.id.tx1);
        tx2 = (TextView) findViewById(R.id.tx2);
        tx3 = (TextView) findViewById(R.id.tx3);
        tx4 = (TextView) findViewById(R.id.tx4);
        tx5 = (TextView) findViewById(R.id.tx5);
        tx6 = (TextView) findViewById(R.id.tx6);
        txBrelan = (TextView) findViewById(R.id.txBrelan);
        txCarre = (TextView) findViewById(R.id.txCarre);
        txFull = (TextView) findViewById(R.id.txFull);
        txPetite = (TextView) findViewById(R.id.txPetite);
        txGrande = (TextView) findViewById(R.id.txGrande);
        txYams = (TextView) findViewById(R.id.txYams);
        txChance = (TextView) findViewById(R.id.txChance);

        txBonus = (TextView) findViewById(R.id.txBonus);
        txScoreSup = (TextView) findViewById(R.id.txTotalH);
        txScoreInf = (TextView) findViewById(R.id.txTotalBas);
        txScoreFinal = (TextView) findViewById(R.id.txTotalF);


        vTextViewSup.add(tx1);
        vTextViewSup.add(tx2);
        vTextViewSup.add(tx3);
        vTextViewSup.add(tx4);
        vTextViewSup.add(tx5);
        vTextViewSup.add(tx6);

        vTextViewInf.add(txBrelan);
        vTextViewInf.add(txCarre);
        vTextViewInf.add(txFull);
        vTextViewInf.add(txPetite);
        vTextViewInf.add(txGrande);
        vTextViewInf.add(txYams);
        vTextViewInf.add(txChance);
//----------------------
//GESTION PERSO DES TOAST
        inflater = getLayoutInflater();
        layout_toast = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_lay));
        text_toast = (TextView) layout_toast.findViewById(R.id.tx_toast);

//--------------------------------------------------------------------------
        Intent getIntent = getIntent();

        yamsM = new YamsManager(this);
        yamsM.open();

        SharedPreferences settings = getSharedPreferences(MenuActivity.PREFS_NAME,0);
// SI on reprend une partie en cours, on charge la partie sauvé dans les prefs
        if(settings.contains("PARTIE_ID_ENCOURS")){
            p = yamsM.getPartie(settings.getInt("PARTIE_ID_ENCOURS",0));
            numTour = settings.getInt("TURN", 1);
            vJoueurs = new Vector<Joueur>();
            for(int i = 0;i < p.getvGrille().size();i++){
                String strKey = "JOUEUR"+i;
                vJoueurs.add(yamsM.getJoueur(settings.getInt(strKey, 0)));
            }
            numJoueurEnCours = settings.getInt("NUMJOUENCOURS",0);
        }else {//SInon on démarre une nouvelle partie avec l'id de la partie passé dans l'intent
            p = yamsM.getPartie(getIntent.getIntExtra("ID_PARTIE", 0));

            vJoueurs = new Vector<Joueur>();
            for (Grille g : p.getvGrille()) {
                vJoueurs.add(yamsM.getJoueur(g.getId_joueur()));
            }
            Collections.shuffle(vJoueurs);
            numJoueurEnCours = 0;
        }

        jouEnCours = vJoueurs.get(numJoueurEnCours);
        nbJoueur = vJoueurs.size();

        for (Grille g : p.getvGrille()) {
            if (g.getId_joueur() == jouEnCours.getId()) {
                grillEnCours = g;
            }
        }
      //  Log.d("POUSS", String.valueOf(jouEnCours.getId()));
      //  Log.d("POUSS", grillEnCours.toString());
        txJouEnCours.setText(jouEnCours.getNom());
//Log.i("POUSS", String.valueOf(p.getvGrille().size()));

        txInfo.setText("Tour " + String.valueOf(numTour));
        afficheGrille(grillEnCours);
        updateTotal();
    }

    //Function : Sauvegarde les données de la partie en cours dans les préférences
    private void saveGame(){
        SharedPreferences settings = getSharedPreferences(MenuActivity.PREFS_NAME, 0);
        SharedPreferences.Editor editorPref = settings.edit();
        editorPref.putInt("TURN", numTour);
        editorPref.putInt("PARTIE_ID_ENCOURS", p.getId());
        //Boucle pour sauvegarder l'ordre des joueurs
        for(int i = 0;i < vJoueurs.size();i++){
            Joueur j = vJoueurs.get(i);
            String strKey = "JOUEUR"+i;
            editorPref.putInt(strKey, j.getId());
        }
        editorPref.putInt("NUMJOUENCOURS",numJoueurEnCours);
        editorPref.putInt("NBJOUEUR", nbJoueur);
        editorPref.apply();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveGame();
    }

    //Function: Gère le check sur un dés;
    //IN: une View qui correpond à l'images checké
    //OUT:
    public void checkBtn(View v) {
        // Log.d("POUSS", "CLickok");
        if (turn != 1 && turn != 4) {
            CheckableImageView des = (CheckableImageView) v;
            des.toggle();
        }
    }
//Vérifie si les boutons pour rentrer un score sont accessibles
    public boolean isScoreAdded() {
        for (Button b : vButtonScore) {
            if (b.isEnabled()) return false;
        }
        return true;
    }
//Lance l’animation pour chacun des dés lancés. Et contrôle le nombre de lancer.
    public void lancerLesDes(View v) {

        if (turn <= 3) {
            playsound(R.raw.des);
            disableButton();
            int nb = getNbDesALancer();
            for (CheckableImageView des : vImgDes) {
                if (!des.isChecked()) {
                    Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_des);
                    des.startAnimation(animation);
                    desAlea(des);
                }
            }
            HashMap<String, Boolean> hashCombi = checkCombi(getCombi());
            enableButton(hashCombi);
            if (turn == 3) {
                checkDice();
                btnBarrer.setEnabled(true);
            }
            turn++;
            changeBtnLancer();
        } else {
            if (isScoreAdded()) {
                changeJoueur();
                uncheckDice();
                turn = 1;
                changeBtnLancer();
            } else {
                text_toast.setText("Veuillez sélectionner un score, Merci.");
                Toast toast = new Toast(getApplicationContext());
                toast.setView(layout_toast);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }
//Modifie le texte du bouton Lancer pour qu’il afficher le numéro du lancer.
    public void changeBtnLancer() {
        switch (turn) {
            case 1:
                btnLancer.setText(R.string.btnLancer);
                break;
            case 2:
                btnLancer.setText(R.string.btnLancer2);
                break;
            case 3:
                btnLancer.setText(R.string.btnLancer3);
                break;
            case 4:
                btnLancer.setText(R.string.btnLancerFin);
                break;
            default:
                btnLancer.setText(R.string.btnLancer);
                break;
        }
    }
//Rend tous les boutons score inaccessibles.
    public void disableButton() {
        for (Button b : vButtonScore) {
            b.setEnabled(false);
        }
    }
//Rend tous les boutons score accessibles afin de pouvoir barrer un score.
    public void setBarre(View v) {
        Button b = (Button) v;
        if(b.getText().toString().equalsIgnoreCase("BARRER")) {
            HashMap<String, Boolean> hashCombi = new HashMap<String, Boolean>();
            hashCombi.put("UN", true);
            hashCombi.put("DEUX", true);
            hashCombi.put("TROIS", true);
            hashCombi.put("QUATRE", true);
            hashCombi.put("CINQ", true);
            hashCombi.put("SIX", true);
            hashCombi.put("BRELAN", true);
            hashCombi.put("CARRE", true);
            hashCombi.put("FULL", true);
            hashCombi.put("YAMS", true);
            hashCombi.put("PETITE", true);
            hashCombi.put("GRANDE", true);
            hashCombi.put("CHANCE", true);
            enableButton(hashCombi);
            boolBarre = true;
            b.setText("ANNULER");
        }else{
            b.setText("BARRER");
            boolBarre = false;
            disableButton();
            enableButton(checkCombi(getCombi()));
        }

    }
//Rend les boutons score accessible quand on peut rentrer un score correspondant. .
    public void enableButton(HashMap<String, Boolean> hashCombi) {
        for (String s : hashCombi.keySet()) {
            if (s.equals("UN") && tx1.getText().toString().isEmpty()) {
                btn1.setEnabled(true);
            }
            if (s.equals("DEUX") && tx2.getText().toString().isEmpty() ) {
                btn2.setEnabled(true);
            }
            if (s.equals("TROIS") && tx3.getText().toString().isEmpty() ) {
                btn3.setEnabled(true);
            }
            if (s.equals("QUATRE") && tx4.getText().toString().isEmpty() ) {
                btn4.setEnabled(true);
            }
            if (s.equals("CINQ") && tx5.getText().toString().isEmpty()  ){
                btn5.setEnabled(true);
            }
            if (s.equals("SIX") && tx6.getText().toString().isEmpty()) {
                btn6.setEnabled(true);
            }
            if (s.equalsIgnoreCase("BRELAN") && txBrelan.getText().toString().isEmpty() ) {
                btnBrelan.setEnabled(true);
            }
            if (s.equalsIgnoreCase("CARRE") && txCarre.getText().toString().isEmpty() ) {
                btnCarre.setEnabled(true);
            }
            if (s.equalsIgnoreCase("FULL") && txFull.getText().toString().isEmpty() ) {
                btnFull.setEnabled(true);
            }
            if (s.equalsIgnoreCase("PETITE") && txPetite.getText().toString().isEmpty() ) {
                btnPetite.setEnabled(true);
            }
            if (s.equalsIgnoreCase("GRANDE") && txGrande.getText().toString().isEmpty() ) {
                btnGrande.setEnabled(true);
            }
            if (s.equalsIgnoreCase("YAMS") && txYams.getText().toString().isEmpty() ) {
                btnYams.setEnabled(true);
            }
            if (s.equalsIgnoreCase("CHANCE") && txChance.getText().toString().isEmpty()) {
                btnChance.setEnabled(true);
            }
        }
    }
//Met le score dans la case correspondante et met à jour la grille correspondante.
    public void setScore(View v) {
        disableButton();
        btnBarrer.setEnabled(false);
        Button btnScore = (Button) v;
        String id = btnScore.getText().toString();

        Vector<Integer> vDes = getCombi();
        int score = 0;
        if (id.equals("1")) {
            for (int i : vDes) {
                if (i == 1) {
                    score = score + 1;
                }
            }
            if(boolBarre){
                tx1.setText("//");
                grillEnCours.setScore_un(66);
            }else {
                tx1.setText(String.valueOf(score));
                grillEnCours.setScore_un(score);
            }
        } else if (id.equals("2")) {
            for (int i : vDes) {
                if (i == 2) {
                    score = score + 2;
                }
            }if(boolBarre){
                tx2.setText("//");
                grillEnCours.setScore_deux(66);
            }else {
                tx2.setText(String.valueOf(score));
                grillEnCours.setScore_deux(score);
            }
        } else if (id.equals("3")) {
            for (int i : vDes) {
                if (i == 3) {
                    score = score + 3;
                }
            }if(boolBarre){
                tx3.setText("//");
                grillEnCours.setScore_trois(66);
            }else {
                tx3.setText(String.valueOf(score));
                grillEnCours.setScore_trois(score);
            }
        } else if (id.equals("4")) {
            for (int i : vDes) {
                if (i == 4) {
                    score = score + 4;
                }
            }
            if (boolBarre) {
                tx4.setText("//");
                grillEnCours.setScore_quatre(66);
            } else {
                tx4.setText(String.valueOf(score));
                grillEnCours.setScore_quatre(score);
            }
        } else if (id.equals("5")) {
            for (int i : vDes) {
                if (i == 5) {
                    score = score + 5;
                }
            }
            if (boolBarre) {
                tx5.setText("//");
                grillEnCours.setScore_cinq(66);
            } else {
                tx5.setText(String.valueOf(score));
                grillEnCours.setScore_cinq(score);
            }
        } else if (id.equals("6")) {
            for (int i : vDes) {
                if (i == 6) {
                    score = score + 6;
                }
            }
            if (boolBarre) {
                tx6.setText("//");
                grillEnCours.setScore_six(66);
            } else {
                tx6.setText(String.valueOf(score));
                grillEnCours.setScore_six(score);
            }
        } else if (id.equalsIgnoreCase("brelan")) {
            for (int i : vDes) {
                score = score + i;
            }
            if (boolBarre) {
                txBrelan.setText("//");
                grillEnCours.setScore_brelan(66);
            } else {
                txBrelan.setText(String.valueOf(score));
                grillEnCours.setScore_brelan(score);
            }
        } else if (id.equalsIgnoreCase("carré")) {
            for (int i : vDes) {
                score = score + i;
            }
            if (boolBarre) {
                txCarre.setText("//");
                grillEnCours.setScore_carre(66);
            } else {
                txCarre.setText(String.valueOf(score));
                grillEnCours.setScore_carre(score);
            }
        } else if (id.equalsIgnoreCase("full")) {
            score = 25;
            if (boolBarre) {
                txFull.setText("//");
                grillEnCours.setScore_full(66);
            } else {
                txFull.setText(String.valueOf(score));
                grillEnCours.setScore_full(score);
            }
        } else if (id.equalsIgnoreCase("petite")) {
            score = 30;
            if (boolBarre) {
                txPetite.setText("//");
                grillEnCours.setScore_petite(66);
            } else {
                txPetite.setText(String.valueOf(score));
                grillEnCours.setScore_petite(score);
            }
        } else if (id.equalsIgnoreCase("grande")) {
            score = 40;
            if (boolBarre) {
                txGrande.setText("//");
                grillEnCours.setScore_grande(66);
            } else {
                txGrande.setText(String.valueOf(score));
                grillEnCours.setScore_grande(score);
            }
        } else if (id.equalsIgnoreCase("yams")) {
            score = 50;
            if (boolBarre) {
                txYams.setText("//");
                grillEnCours.setScore_yams(66);
            } else {
                txYams.setText(String.valueOf(score));
                grillEnCours.setScore_yams(score);
            }
        } else if (id.equalsIgnoreCase("chance")) {
            for (int i : vDes) {
                score = score + i;
            }
            txChance.setText(String.valueOf(score));
            grillEnCours.setScore_chance(score);
        }
        updateTotal();
        if(turn <=3) {
             //changeJoueur();
             //uncheckDice();
             turn = 4;
             changeBtnLancer();
        }
        boolBarre = false;
        btnBarrer.setText("BARRER");
    }


    //Function : Genere un nombre aléatoire et modifie l'image associé du dés
    //IN: un objet CheckableImageView qui représente un dés
    public void desAlea(CheckableImageView des) {
        int alea = (int) (Math.random() * 6 + 1);
        int id = getResources().getIdentifier("des" + alea, "drawable", getPackageName());
        des.setImageResource(id);
        des.setTag(alea);
    }

    //Function : Récupère le nombre correspondant à chaque dé.
    // IN: Elle retourne un tableau contenant le score de chaque dé.
    public Vector<Integer> getCombi() {
        Vector<Integer> vDes = new Vector<Integer>();
        String s = "";
        for (CheckableImageView des : vImgDes) {
            s = s + des.getTag();
            vDes.add((Integer) des.getTag());
        }
        return vDes;
    }
    //Function : Examine la combinaison des résultats des 5 dés.
    //IN : tableau contenant le score de chaque dé.
    //OUT : un tableau associatif comprenant les combinaisons possibles avec les dés obtenu
    public HashMap<String, Boolean> checkCombi(Vector<Integer> vDes) {
        HashMap<String, Boolean> hashCombi = new HashMap<String, Boolean>();
        for (int di = 0; di < vDes.size(); di++) {
            if (vDes.get(di) == 1) {
                hashCombi.put("UN", true);
            }
            if (vDes.get(di) == 2) {
                hashCombi.put("DEUX", true);
            }
            if (vDes.get(di) == 3) {
                hashCombi.put("TROIS", true);
            }
            if (vDes.get(di) == 4) {
                hashCombi.put("QUATRE", true);
            }
            if (vDes.get(di) == 5) {
                hashCombi.put("CINQ", true);
            }
            if (vDes.get(di) == 6) {
                hashCombi.put("SIX", true);
            }
        }
        //--------BRELAN--CARRE--YAMS-------------
        String str = "";
        int count = 0;
        boolean brln = false;
        boolean crr = false;
        boolean yms = false;
        boolean fll1 = false;
        boolean fll2 = false;
        for (int i : vDes) {
            str = str.concat(String.valueOf(i));
        }
        for (int i : vDes) {
            String strTemp = str;
            count = strTemp.length() - strTemp.replace(String.valueOf(i), "").length();
            if (count >= 3) brln = true;
            if (count >= 4) crr = true;
            if (count == 5) yms = true;
            if (count == 3) fll1 = true;
            if (count == 2) fll2 = true;
            //Log.d("POUSS", String.valueOf(i) + " : " + String.valueOf(count));
        }
        if (brln) {
            hashCombi.put("BRELAN", true);
        }
        if (crr) {
            hashCombi.put("CARRE", true);
        }
        if (yms) {
            hashCombi.put("YAMS", true);
            playsound(R.raw.fanfare);
        }
        if ((fll1 && fll2) || yms) {
            hashCombi.put("FULL", true);
        }
        //--------------------SUITE------------------
        Collections.sort(vDes);

        int suite = 1;
        for (int i = 0; i < vDes.size() - 1; i++) {
            if (vDes.get(i) + 1 == vDes.get(i + 1)) {
                suite++;
            }else{

                if(vDes.get(i) == vDes.get(i+1) ){

                }else{
                    if(i>0 && i<3) {
                        suite--;
                    }
                }
            }
//Log.d("POUSS", String.valueOf(vDes.get(i)) + "/" + String.valueOf(vDes.get(i + 1)));
//Log.d("POUSS", String.valueOf(suite));
        }
        if (suite >= 4) {
            hashCombi.put("PETITE", true);
        }
        if (suite == 5) {
            hashCombi.put("GRANDE", true);
        }
        hashCombi.put("CHANCE", true);
        return hashCombi;
    }

    //Function: Sélectionne tous les dés
    public void checkDice() {
        for (CheckableImageView des : vImgDes) {
            des.setChecked(true);
        }
    }

    //Function: Désélectionne tous les dés
    public void uncheckDice() {
        for (CheckableImageView des : vImgDes) {
            des.setChecked(false);
            des.setImageResource(R.drawable.des);
        }
    }

    //Function: Gere le changement de joueur
    //IN:
    //OUT:
    public void changeJoueur() {
        boolean fini = false;
        yamsM.updateGrille(grillEnCours);
        numJoueurEnCours++;
        if (numJoueurEnCours > nbJoueur - 1) {
            numJoueurEnCours = 0;
            numTour++;
            if (numTour > nbTourJeu) {
                fini = true;
                //Toast.makeText(this, "Partie terminé!!", Toast.LENGTH_SHORT).show();
               // affichFin();
            } else {
                txInfo.setText("Tour " + String.valueOf(numTour));
            }
        }
        jouEnCours = vJoueurs.get(numJoueurEnCours);
        for (Grille g : p.getvGrille()) {
            if (g.getId_joueur() == jouEnCours.getId()) {
                grillEnCours = g;
            }
        }
       // Log.d("POUSS", String.valueOf(jouEnCours.getId()));
       // Log.d("POUSS", grillEnCours.toString());
        txJouEnCours.setText(jouEnCours.getNom());
        afficheGrille(grillEnCours);
        updateTotal();
        if(fini){
          affichFin();
        }
    }
    //Function : Affiche une boite de dialogue avec les résultats
    private void affichFin(){
        SharedPreferences settings = getSharedPreferences(MenuActivity.PREFS_NAME, 0);

        SharedPreferences.Editor editor = settings.edit();
        editor.remove("PARTIE_ID_ENCOURS");
        editor.remove("TURN");
        editor.remove("NUMJOUENCOURS");
        editor.remove("NBJOUEUR");
        for(int i = 0;i < vJoueurs.size();i++){
            String strKey = "JOUEUR"+i;
            editor.remove(strKey);
        }
        editor.apply();


        playsound(R.raw.applause);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.titlefin);
        String msg = "";
        int scoreMax =0;
        String vainqueur = "";
        for(Grille g:p.getvGrille()){
            msg = msg+g.getNomJoueur(yamsM)+" : "+ String.valueOf(g.getScore_totalBas()+g.getScore_totalHaut())+"\n";
            if (g.getScore_totalBas()+g.getScore_totalHaut()>scoreMax){
                vainqueur = g.getNomJoueur(yamsM);
                scoreMax = g.getScore_totalBas()+g.getScore_totalHaut();
            }
        }
        msg = msg+"Félicitation à "+vainqueur+" pour cette victoire.\nHonte aux autres...";
        builder.setMessage(msg)
        .setCancelable(false)
        .setPositiveButton("Accueil", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getBaseContext(), MenuActivity.class);
                startActivity(intent);
                GameActivity.this.finish();
            }
        })
        .setNegativeButton("Quitter", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.finish();
            }
        }).setNeutralButton("Voir les grilles", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getBaseContext(), GrilleActivity.class);
                intent.putExtra("IDPARTIE", p.getId());
                intent.putExtra("PARTIFINI", true);
                startActivity(intent);
                GameActivity.this.finish();
            }
        });



        AlertDialog dialog = builder.create();
        dialog.show();



    }
    //Function : Affiche les résultat d'une grille dans les textview correspondantes
    //IN : Un objet grille
    private void afficheGrille(Grille g) {
        if (g.getScore_un() == BARRE) {
            tx1.setText("//");
        } else if(g.getScore_un() != 0){
            tx1.setText(String.valueOf(g.getScore_un()));
        }else {
            tx1.setText("");
        }
        if (g.getScore_deux() == BARRE) {
            tx2.setText("//");
        }else if(g.getScore_deux() != 0){
            tx2.setText(String.valueOf(g.getScore_deux()));
        } else {
            tx2.setText("");
        }
        if (g.getScore_trois() == BARRE) {
            tx3.setText("//");
        } else if(g.getScore_trois() != 0){
            tx3.setText(String.valueOf(g.getScore_trois()));
        }else {
            tx3.setText("");
        }
        if (g.getScore_quatre() == BARRE) {
            tx4.setText("//");
        } else if(g.getScore_quatre() != 0){
             tx4.setText(String.valueOf(g.getScore_quatre()));
        }else {
            tx4.setText("");
        }
        if (g.getScore_cinq() == BARRE) {
            tx5.setText("//");
        } else if(g.getScore_cinq() != 0){
            tx5.setText(String.valueOf(g.getScore_cinq()));
        }else {
            tx5.setText("");
        }
        if (g.getScore_six() == BARRE) {
            tx6.setText("//");
        } else if(g.getScore_six() != 0){
            tx6.setText(String.valueOf(g.getScore_six()));
        }else {
            tx6.setText("");
        }
        if (g.getScore_brelan() == BARRE) {
            txBrelan.setText("//");
        } else if(g.getScore_brelan() != 0){
            txBrelan.setText(String.valueOf(g.getScore_brelan()));
        }else {
            txBrelan.setText("");
        }
        if (g.getScore_carre() == BARRE) {
            txCarre.setText("//");
        } else if(g.getScore_carre() != 0){
            txCarre.setText(String.valueOf(g.getScore_carre()));
        }else {
            txCarre.setText("");
        }
        if (g.getScore_full() == BARRE) {
            txFull.setText("//");
        } else if(g.getScore_full() != 0){
            txFull.setText(String.valueOf(g.getScore_full()));
        }else {
            txFull.setText("");
        }
        if (g.getScore_petite() == BARRE) {
            txPetite.setText("//");
        } else if(g.getScore_petite() != 0){
            txPetite.setText(String.valueOf(g.getScore_petite()));
        }else {
            txPetite.setText("");
        }
        if (g.getScore_grande() == BARRE) {
            txGrande.setText("//");
        } else if(g.getScore_grande() != 0){
            txGrande.setText(String.valueOf(g.getScore_grande()));
        }else {
            txGrande.setText("");
        }
        if (g.getScore_yams() == BARRE) {
            txYams.setText("//");
        } else if(g.getScore_yams() != 0){
            txYams.setText(String.valueOf(g.getScore_yams()));
        }else {
            txYams.setText("");
        }
        if (g.getScore_chance() == BARRE) {
            txChance.setText("//");
        } else if(g.getScore_chance() != 0){
            txChance.setText(String.valueOf(g.getScore_chance()));
        }else {
            txChance.setText("");
        }
    }

    //Function: Met à jour le total supérieur, le total inférieur et le total complet
    private void updateTotal() {
        int totalSup = 0;
        int totalInf = 0;
        int bonus = 0;
        for (TextView t : vTextViewSup) {
            if (!t.getText().toString().isEmpty() && !t.getText().toString().equals("//")) {
                totalSup = totalSup + Integer.parseInt(t.getText().toString());
            }
        }
        for (TextView t : vTextViewInf) {
            if (!t.getText().toString().isEmpty() && !t.getText().toString().equals("//")) {
                totalInf = totalInf + Integer.parseInt(t.getText().toString());
            }
        }
        if (totalSup >= SCORE_BONUS) {
            txBonus.setText("35");
            bonus = 35;
        }else{
            txBonus.setText("");
        }
        txScoreSup.setText(String.valueOf(totalSup));
        txScoreInf.setText(String.valueOf(totalInf));
        txScoreFinal.setText(String.valueOf(totalInf + totalSup + bonus));
        grillEnCours.setScore_totalHaut(totalSup + bonus);
        grillEnCours.setScore_totalBas(totalInf);
        yamsM.updateGrille(grillEnCours);
    }

    public void launchGrilleScore(View v){
        Intent intent = new Intent(this, GrilleActivity.class);
        intent.putExtra("IDPARTIE", p.getId());
        startActivity(intent);
    }
    public int getNbDesALancer() {
        int nb = 0;
        for (CheckableImageView des : vImgDes) {
            if (!des.isChecked()) {
                nb++;
            }
        }
        return nb;
    }
    //Function: Joue un son dont l'identifiant est passé en paramètre.
    private void playsound(int resID){
        MediaPlayer mp = new MediaPlayer();
        if(mp != null) {
            if(mp.isPlaying()) {
                mp.stop();
            }
            mp.release();
        }
        mp = MediaPlayer.create(this, resID);
        mp.seekTo(1500);
        mp.start();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Le Yams de Pouss");
        builder.setMessage("Voulez vous quitter la partie?")
                .setCancelable(true)
                .setPositiveButton("Accueil", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveGame();
                        Intent intent = new Intent(getBaseContext(), MenuActivity.class);
                        startActivity(intent);
                        GameActivity.this.finish();
                    }
                })
                .setNegativeButton("Quitter", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveGame();
                        GameActivity.this.finish();
                    }
                }).setNeutralButton("Continuer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });;
        builder.setIcon(R.mipmap.ic_launcher);
        AlertDialog dialog = builder.create();
        dialog.show();

    }
}
