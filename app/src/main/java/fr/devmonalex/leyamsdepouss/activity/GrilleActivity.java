package fr.devmonalex.leyamsdepouss.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Vector;

import fr.devmonalex.leyamsdepouss.R;
import fr.devmonalex.leyamsdepouss.modele.Grille;
import fr.devmonalex.leyamsdepouss.modele.Joueur;
import fr.devmonalex.leyamsdepouss.modele.Partie;
import fr.devmonalex.leyamsdepouss.modele.YamsManager;

/**
 * Created by dl211 on 14/10/2015.
 */
public class GrilleActivity extends AppCompatActivity {
    private final static int NOMBRE_DE_ROW = 18;
    private final static int SCORE_BARRE = 66;
    private final static String AFFICHE_BARRE = "//";

    private TableLayout table;
    private Partie partie;
    private Vector<Grille> vGrille;
    private Vector<Joueur> vJoueur;
    private Vector<String> vString = new Vector<String>();
    private YamsManager yamsM;
    private Intent getIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grille_layout);
        //Ouverture base de données
        yamsM = new YamsManager(this);
        yamsM.open();
        //pour recupere la taille de l'ecran
        Display ecran = getWindowManager().getDefaultDisplay();
        int largeur = ecran.getWidth();
        int hauteur = ecran.getHeight();
        Button btn = (Button) findViewById(R.id.btnReturn);

        int hauteurCase = (hauteur-btn.getHeight()) / (NOMBRE_DE_ROW + 2);
        //Recup des grille à afficher
        getIn = getIntent();
        if(getIn.hasExtra("PARTIFINI")){
            btn.setText("Accueil");
        }
        if(getIn.hasExtra("SCORE")){
            btn.setText("Retour");
        }
        partie = yamsM.getPartie(getIn.getIntExtra("IDPARTIE", 0));
        vGrille = partie.getvGrille();

        AjoutString();

        table = (TableLayout) findViewById(R.id.tableGrille);

        for (int i = 0; i < NOMBRE_DE_ROW; i++) {
            TableRow row = new TableRow(this);
            for (int j = 0; j <= vGrille.size(); j++) {
                TextView t = new TextView(this);
                if (j == 0) {

                    t.setText(vString.get(i));
                } else {
                    Grille g = vGrille.get(j - 1);
                   // TextView t = new TextView(this);
                    if (i == 0) {//Premiere ligne : on affiche le nom des joueurs
                        t.setText(g.getNomJoueur(yamsM));
                    } else if (i == NOMBRE_DE_ROW - 1) {
                        t.setText(String.valueOf(g.getScore_totalBas() + g.getScore_totalHaut()));
                    } else {
                        if (g.scoreToTab()[i - 1] == SCORE_BARRE) {
                            t.setText(AFFICHE_BARRE);
                        } else {
                            t.setText((String.valueOf(g.scoreToTab()[i - 1])));
                        }
                    }
                }
                row.addView(t); //Attach TextView to its parent (row)
                TableRow.LayoutParams params =
                        (TableRow.LayoutParams) t.getLayoutParams();
                params.setMargins(0, 0, 0, 0);
                params.height = hauteurCase;
                t.setGravity(Gravity.END | Gravity.CENTER_VERTICAL);
                t.setPadding(5, 0, 5, 0);
                t.setBackground(getResources().getDrawable(R.drawable.cadre_grille));


            }
            table.addView(row);
        }
    }

    public void retour(View v){
        if(getIn.hasExtra("PARTIFINI")){
            Intent accueilIntent = new Intent(this, MenuActivity.class);
            startActivity(accueilIntent);
            finish();
        }else if(getIn.hasExtra("SCORE")){
            Intent accueilIntent = new Intent(this, ScoreActivity.class);
            startActivity(accueilIntent);
            finish();

        }else{
        finish();}
    }

    private void AjoutString() {
        vString.add("Nom");
        vString.add("Un");
        vString.add("Deux");
        vString.add("Trois");
        vString.add("Quatre");
        vString.add("Cinq");
        vString.add("Six");
        vString.add("Bonus");
        vString.add("Total Sup");
        vString.add("Brelan");
        vString.add("Carré");
        vString.add("Full");
        vString.add("Petite");
        vString.add("Grande");
        vString.add("Yams");
        vString.add("Chance");
        vString.add("Total Inf");
        vString.add("Total Final");
    }
}
