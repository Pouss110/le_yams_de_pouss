package fr.devmonalex.leyamsdepouss.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import fr.devmonalex.leyamsdepouss.R;

public class MenuActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "MyPrefsFile";

    private String TAG = "POUSS";
    private ImageView des1,des2,des3,des4,des5,des6;

    private Button btnReprise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        //Initialisation des vues
        des1 = (ImageView) findViewById(R.id.imageView);
        des2 = (ImageView) findViewById(R.id.imageView2);
        des3 = (ImageView) findViewById(R.id.imageView3);
        des4 = (ImageView) findViewById(R.id.imageView4);
        des5 = (ImageView) findViewById(R.id.imageView5);
        des6 = (ImageView) findViewById(R.id.imageView6);
        launchAnim();

        btnReprise = (Button) findViewById(R.id.btnReprendre);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME,0);
        btnReprise.setEnabled(false);
        if(settings.contains("PARTIE_ID_ENCOURS")){
            if(settings.getInt("TURN",0)<14) {
                btnReprise.setEnabled(true);
            }
        }

    }
    public void reprendrePartie(View v){
        Intent newGame = new Intent(this, GameActivity.class);
        startActivity(newGame);
        finish();
    }
    //Lance l'activité choix des joueurs
    public void newGame(View v){
        Intent newGame = new Intent(this, PlayerActivity.class);
        startActivity(newGame);
        finish();
    }
    //Lance l'animation des imageView
    private void launchAnim(){
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.anim_menu);
        des1.startAnimation(animation);
        des2.startAnimation(animation);
        des3.startAnimation(animation);
        des4.startAnimation(animation);
        des5.startAnimation(animation);
        des6.startAnimation(animation);
    }

    public void afficheMaxScore(View v){
        Intent newGame = new Intent(this, ScoreActivity.class);
        startActivity(newGame);
        finish();
    }
}
