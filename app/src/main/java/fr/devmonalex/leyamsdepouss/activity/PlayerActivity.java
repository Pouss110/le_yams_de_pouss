package fr.devmonalex.leyamsdepouss.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import fr.devmonalex.leyamsdepouss.R;
import fr.devmonalex.leyamsdepouss.modele.Grille;
import fr.devmonalex.leyamsdepouss.modele.Joueur;
import fr.devmonalex.leyamsdepouss.modele.Partie;
import fr.devmonalex.leyamsdepouss.modele.YamsManager;

/**
 * Created by dl211 on 06/10/2015.
 */
public class PlayerActivity extends AppCompatActivity {
    private int MODE_ADD_PLAYER = 0;
    private int MODE_MODIFY_PLAYER = 1;

    YamsManager yamsM;
    GridView gridView;
    Vector<Joueur> vj;
    ArrayList<HashMap<String,String>> listJoueur = new ArrayList<HashMap<String, String>>();
    SimpleAdapter adapter;

    private LayoutInflater inflater;
    private View layout_toast;
    private TextView text_toast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_player);
        //On ouvre et on se connecte à la base de données
        yamsM = new YamsManager(this);
        yamsM.open();

       //à enlever quand test fini
       // yamsM.delGrille();

        //On initialise la gridview
        gridView = (GridView) findViewById(R.id.gridPlayer);
        //On récupère la liste des joueur
        Vector<Joueur> vj = yamsM.getAllJoueur();

        //GESTION PERSO DES TOAST
        inflater = getLayoutInflater();
        layout_toast = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_lay));
        text_toast = (TextView) layout_toast.findViewById(R.id.tx_toast);


        //SI auncun joueur n'est présent, on affiche un message pour créer un nouveau joueur.
        if(vj.size()==0){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("C'est votre première fois!").setCancelable(false);
            builder.setMessage("Pour commencer, créer un joueur!");
            builder.setIcon(R.mipmap.ic_launcher);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent addIntent = new Intent(getBaseContext(), ManagePlayerActivity.class);
                    addIntent.putExtra("MODE", MODE_ADD_PLAYER);

                    startActivity(addIntent);
                    finish();
                }
            });


            AlertDialog dlg = builder.create();
            dlg.show();
        }
        for(Joueur j : vj){
            listJoueur.add(j.toHash());
        }
        //Adapter la liste à la gridview
        adapter = new SimpleAdapter(this, listJoueur,
                R.layout.player_item, new String[]{"id_joueur","nom_joueur"}, new int[]{R.id.id_joueur,R.id.nom_joueur});

        gridView.setAdapter(adapter);
        gridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Intent addIntent = new Intent(getBaseContext(), ManagePlayerActivity.class);
                addIntent.putExtra("MODE", MODE_MODIFY_PLAYER);
                HashMap<String,String> mp = (HashMap<String, String>) gridView.getItemAtPosition(position);
//Log.d("POUSS", mp.get("nom_joueur"));
                addIntent.putExtra("ID",mp.get("id_joueur"));
                startActivity(addIntent);
                finish();
                return true;
            }
        });




    }

    //Méthode du lancement du jeu
    public void launchGame(View v){

        //on crée une nouvelle partie
        Partie p = new Partie();

        //on crée une grille pour chaque joueur
        getGrilleFromPlayer(p);
        if(p.getvGrille().size()==0){
            text_toast.setText("Merci de sélectionner un joueur.");
            Toast toast = new Toast(getApplicationContext());
            toast.setView(layout_toast);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
        }else {
            //on insere la partie dans la base de données
            p.setId((int)yamsM.insertNewPartie(p));
            //et on insere dans la base la relation grille partie
            yamsM.insertGrilleIntoPartie(p);
//Log.d("POUSS", String.valueOf(p.getId()));
//Log.d("POUSS", String.valueOf(p.getId()));

            SharedPreferences settings = getSharedPreferences(MenuActivity.PREFS_NAME, 0);

            SharedPreferences.Editor editor = settings.edit();
            editor.remove("PARTIE_ID_ENCOURS");
            editor.remove("TURN");
            editor.remove("NUMJOUENCOURS");
            int nb=0;
            if(settings.contains("NBJOUEUR")) {
                nb = settings.getInt("NBJOUEUR",0);
            }
            for(int i = 0;i < nb;i++){
                String strKey = "JOUEUR"+i;
                editor.remove(strKey);
            }
            editor.remove("NBJOUEUR");
            editor.commit();

            Intent launchIntent = new Intent(this, GameActivity.class);
            launchIntent.putExtra("ID_PARTIE", p.getId());
            startActivity(launchIntent);
            finish();
        }
    }
    //Function : Récupere les joueurs selectionnées et crée pour chacun d'eux une grille qu'on
    //           insere dans la bdd
    //IN: La partie qui sera utilisé
    private void getGrilleFromPlayer(Partie p){
        Vector<Grille> v = new Vector<Grille>();
        //on recupere le statut de chaque joueur affiché
        SparseBooleanArray a = gridView.getCheckedItemPositions();
        for(int i = 0;i<a.size();i++){
            if(a.valueAt(i)){
                int pos =  a.keyAt(i);
                HashMap<String,String> map = (HashMap<String,String>) adapter.getItem(pos);
//Log.d("POUSS", map.get("id_joueur") + "//" + map.get("nom_joueur"));
                Grille g = new Grille(Integer.parseInt(map.get("id_joueur")));
                g.setId((int)yamsM.insertNewGrille(g));
                v.add(g);
            }
        }
        p.setvGrille(v);
    }

    //Méthode du bouton Ajouter Joueur
    public void addPlayer(View v){
        Intent addIntent = new Intent(this, ManagePlayerActivity.class);
        addIntent.putExtra("MODE",MODE_ADD_PLAYER);

        startActivity(addIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent menuIntent = new Intent(this, MenuActivity.class);
        startActivity(menuIntent);
        finish();
    }
}
