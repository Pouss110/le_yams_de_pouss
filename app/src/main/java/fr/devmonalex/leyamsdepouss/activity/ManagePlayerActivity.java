package fr.devmonalex.leyamsdepouss.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Vector;

import fr.devmonalex.leyamsdepouss.R;
import fr.devmonalex.leyamsdepouss.modele.Joueur;
import fr.devmonalex.leyamsdepouss.modele.YamsManager;

/**
 * Created by dl211 on 07/10/2015.
 */
public class ManagePlayerActivity extends AppCompatActivity {
    private EditText editNomJoueur;
    private Button btnManageJoueur;
    private int mode;
    private YamsManager yamsM;
    private Vector<Joueur> vj;
    private int id_joueur;
    private Joueur joueur;

    private LayoutInflater inflater;
    private View layout_toast;
    private TextView text_toast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_player_layout);

        editNomJoueur = (EditText) findViewById(R.id.editPlayer);
        btnManageJoueur = (Button) findViewById(R.id.btnManagePlayer);

        Intent getIntent = getIntent();
        mode = getIntent.getIntExtra("MODE",0);
        //Initialise l'accés a la base
        yamsM = new YamsManager(this);
        yamsM.open();
        //Liste des joueur existant
        vj = yamsM.getAllJoueur();

        if(mode == 1){
            id_joueur = Integer.parseInt(getIntent.getStringExtra("ID"));
            joueur = yamsM.getJoueur(id_joueur);
            editNomJoueur.setText(joueur.getNom());
            btnManageJoueur.setText("Modifier");
        }

        //GESTION PERSO DES TOAST
        inflater = getLayoutInflater();
        layout_toast = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_lay));
        text_toast = (TextView) layout_toast.findViewById(R.id.tx_toast);

    }
    //Function qui ajoute un joueur ou le modifie
    public void managePlayer(View v){
        if(validate()){
            if(mode == 0){
                Joueur joueur = new Joueur(editNomJoueur.getText().toString().trim());
                yamsM.insertJoueur(joueur);
                retourChoixJoueur(this.getCurrentFocus());
            }else if(mode == 1){
                joueur.setNom(editNomJoueur.getText().toString().trim());
                yamsM.modifJoueur(joueur);
                retourChoixJoueur(this.getCurrentFocus());
            }
        }
    }
//Contrôle le champ du nom de joueur et vérifie sa validité. Elle retourne Vraie si le nom est correct, Faux si le nom est vide ou déjà pris.
    public boolean validate(){
        if(editNomJoueur.getText().toString().trim().isEmpty()){
            text_toast.setText("Merci de remplir le nom du joueur.");
            Toast toast = new Toast(getApplicationContext());
            toast.setView(layout_toast);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
            return false;
        }
        for(Joueur j:vj){
            if(editNomJoueur.getText().toString().trim().equals(j.getNom())){
                if(mode == 1){
                    if(editNomJoueur.getText().toString().trim().equals(joueur.getNom())){
                        text_toast.setText("Aucun modification effectué.");
                        Toast toast = new Toast(getApplicationContext());
                        toast.setView(layout_toast);
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                        return true;
                    }else{
                        text_toast.setText("Ce nom de joueur est déja pris, Merci d'en choisir un autre.");
                        Toast toast = new Toast(getApplicationContext());
                        toast.setView(layout_toast);
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                        return false;
                    }
                }else {
                    text_toast.setText("Ce nom de joueur est déja pris, Merci d'en choisir un autre.");
                    Toast toast = new Toast(getApplicationContext());
                    toast.setView(layout_toast);
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return false;
                }
            }
        }
        return true;
    }

    //Function retour à l'activité choix du joueur
    public void retourChoixJoueur(View v){
        Intent retourIntent = new Intent(this, PlayerActivity.class);
        startActivity(retourIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        retourChoixJoueur(this.getCurrentFocus());
    }
}
