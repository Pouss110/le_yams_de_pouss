package fr.devmonalex.leyamsdepouss.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import fr.devmonalex.leyamsdepouss.R;
import fr.devmonalex.leyamsdepouss.modele.Grille;
import fr.devmonalex.leyamsdepouss.modele.Joueur;
import fr.devmonalex.leyamsdepouss.modele.YamsManager;

/**
 * Created by dl211 on 21/10/2015.
 */
public class ScoreActivity extends AppCompatActivity {
    private YamsManager yamsM;
    private GridView gridView;
    Vector<Joueur> vj;
    ArrayList<HashMap<String,String>> listJoueur = new ArrayList<>();
    SimpleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.score_activity);

        yamsM = new YamsManager(this);
        yamsM.open();

        gridView = (GridView) findViewById(R.id.gridPlayerSc);

        vj = yamsM.getAllJoueur();

        for(Joueur j : vj){
            Vector<Grille> vg = yamsM.getAllGrilleJoueur(j);
            Grille gMax = getGrilleScoreMax(vg);
            HashMap<String,String> map = j.toHash();
            if(gMax == null){
                //Si le joueur n'as pas effectué de partie, on ne l'affiche pas dans la liste de highScore
            }else {
                map.put("score_joueur", String.valueOf(gMax.getScore_totalBas() + gMax.getScore_totalHaut()));
                map.put("id_grille", String.valueOf(gMax.getId()));
                listJoueur.add(map);
            }

        }

        //Adapter la liste à la gridview
        adapter = new SimpleAdapter(this, listJoueur,
                R.layout.player_score_item, new String[]{"id_joueur","nom_joueur","score_joueur","id_grille"}, new int[]{R.id.id_joueur_sc,R.id.nom_joueur_sc,R.id.score_joueur,R.id.id_grille_sc});

        gridView.setAdapter(adapter);
//Gere le clique sur chaque joueur pour afficher la partie correspondante
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent addIntent = new Intent(getBaseContext(), GrilleActivity.class);
                HashMap<String, String> map = (HashMap<String,String>) gridView.getItemAtPosition(position);
                int id_partie = yamsM.getIdPartie(yamsM.getGrille(Integer.parseInt(map.get("id_grille"))));
                addIntent.putExtra("IDPARTIE", id_partie);
                addIntent.putExtra("SCORE",0);
                startActivity(addIntent);
                finish();
            }
        });
    }
    //Function: Recupère la grille ou le score est au maximum
    //IN: Un vecteur de d'objet Grille
    //OUT: un objet Grille
    private Grille getGrilleScoreMax(Vector<Grille> vg) {
        if(vg.size()==0){
            return null;
        }else {
            Grille g = vg.elementAt(0);
            for (Grille k : vg) {
                int score = k.getScore_totalBas() + k.getScore_totalHaut();
                int maxScore = g.getScore_totalBas() + g.getScore_totalHaut();
                if (score > maxScore) {
                    g = k;
                }
            }
            return g;
        }
    }


    @Override
    public void onBackPressed() {
        Intent menuIntent = new Intent(this, MenuActivity.class);
        startActivity(menuIntent);
        this.finish();
    }
}
