package fr.devmonalex.leyamsdepouss.modele;

/**
 * Created by dl211 on 06/10/2015.
 */
public class Grille {
    private int id;
    private int id_joueur;
    private int score_un;
    private int score_deux;
    private int score_trois;
    private int score_quatre;
    private int score_cinq;
    private int score_six;
    private int score_brelan;
    private int score_carre;
    private int score_chance;
    private int score_full;
    private int score_petite;
    private int score_grande;
    private int score_yams;
    private int score_totalHaut;
    private int score_totalBas;

    public Grille(int id_joueur) {
        this.setId_joueur(id_joueur);
    }

    //Function: Recupere le nom du joueur associé à la grille
    //IN: Un objet YamsManager
    //OUT: une string repésentant le nom du joueur
    public String getNomJoueur(YamsManager yM) {
        Joueur j = yM.getJoueur(this.getId_joueur());
        return j.getNom();
    }

    //---------------------------------GETTER AND SETTER-------------------------------
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_joueur() {
        return id_joueur;
    }

    public void setId_joueur(int id_joueur) {
        this.id_joueur = id_joueur;
    }

    public int getScore_un() {
        return score_un;
    }

    public void setScore_un(int score_un) {
        this.score_un = score_un;
    }

    public int getScore_deux() {
        return score_deux;
    }

    public void setScore_deux(int score_deux) {
        this.score_deux = score_deux;
    }

    public int getScore_trois() {
        return score_trois;
    }

    public void setScore_trois(int score_trois) {
        this.score_trois = score_trois;
    }

    public int getScore_quatre() {
        return score_quatre;
    }

    public void setScore_quatre(int score_quatre) {
        this.score_quatre = score_quatre;
    }

    public int getScore_cinq() {
        return score_cinq;
    }

    public void setScore_cinq(int score_cinq) {
        this.score_cinq = score_cinq;
    }

    public int getScore_six() {
        return score_six;
    }

    public void setScore_six(int score_six) {
        this.score_six = score_six;
    }

    public int getScore_brelan() {
        return score_brelan;
    }

    public void setScore_brelan(int score_brelan) {
        this.score_brelan = score_brelan;
    }

    public int getScore_carre() {
        return score_carre;
    }

    public void setScore_carre(int score_carre) {
        this.score_carre = score_carre;
    }

    public int getScore_chance() {
        return score_chance;
    }

    public void setScore_chance(int score_chance) {
        this.score_chance = score_chance;
    }

    public int getScore_full() {
        return score_full;
    }

    public void setScore_full(int score_full) {
        this.score_full = score_full;
    }

    public int getScore_petite() {
        return score_petite;
    }

    public void setScore_petite(int score_petite) {
        this.score_petite = score_petite;
    }

    public int getScore_grande() {
        return score_grande;
    }

    public void setScore_grande(int score_grande) {
        this.score_grande = score_grande;
    }

    public int getScore_yams() {
        return score_yams;
    }

    public void setScore_yams(int score_yams) {
        this.score_yams = score_yams;
    }

    public int getScore_totalHaut() {
        return score_totalHaut;
    }

    public void setScore_totalHaut(int score_totalHaut) {
        this.score_totalHaut = score_totalHaut;
    }

    public int getScore_totalBas() {
        return score_totalBas;
    }

    public void setScore_totalBas(int score_totalBas) {
        this.score_totalBas = score_totalBas;
    }

    @Override
    public String toString() {
        return "Grille{" +
                "id=" + id +
                ", id_joueur=" + id_joueur +
                ", score_un=" + score_un +
                ", score_deux=" + score_deux +
                ", score_trois=" + score_trois +
                ", score_quatre=" + score_quatre +
                ", score_cinq=" + score_cinq +
                ", score_six=" + score_six +
                ", score_brelan=" + score_brelan +
                ", score_carre=" + score_carre +
                ", score_chance=" + score_chance +
                ", score_full=" + score_full +
                ", score_petite=" + score_petite +
                ", score_grande=" + score_grande +
                ", score_yams=" + score_yams +
                ", score_totalHaut=" + score_totalHaut +
                ", score_totalBas=" + score_totalBas +
                '}';
    }

    public int[] scoreToTab(){
        int[] tabScore = new int[16];
        tabScore[0] = score_un;
        tabScore[1] = score_deux;
        tabScore[2] = score_trois;
        tabScore[3] = score_quatre;
        tabScore[4] = score_cinq;
        tabScore[5] = score_six;
        if(score_totalHaut>62){
            tabScore[6] = 35;
        }else{
            tabScore[6] = 66;
        }
        if(score_totalHaut>62){
            tabScore[7] = score_totalHaut-35;
        }else{
            tabScore[7] = score_totalHaut;
        }
        tabScore[8] = score_brelan;
        tabScore[9] = score_carre;
        tabScore[10] = score_full;
        tabScore[11] = score_petite;
        tabScore[12] = score_grande;
        tabScore[13] = score_yams;
        tabScore[14] = score_chance;
        tabScore[15] = score_totalBas;
        return tabScore;
    }
}
