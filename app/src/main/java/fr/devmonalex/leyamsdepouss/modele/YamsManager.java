package fr.devmonalex.leyamsdepouss.modele;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.Vector;

/**
 * Created by dl211 on 05/10/2015.
 */
public class YamsManager {
    private static final String NOM_BDD = "yams.db";
    private static final int VERSION = 1;

    private SQLiteDatabase bdd;
    private BddSQLite maBaseSQLite;

    public YamsManager(Context context) {
        maBaseSQLite = new BddSQLite(context, NOM_BDD, null, VERSION);
    }

    //Ouvre la base en écriture
    public void open() {
        bdd = maBaseSQLite.getWritableDatabase();
    }

//-------------------FONCTIONS LIES A OBJET JOUEUR-------------------------------------------------

    //Function : Insere un objet joueur dans la base de données.
    //IN : Un objet de la classe joueur
    //OUT : l'id du nouvel enregistrement
    public long insertJoueur(Joueur j) {
        ContentValues values = new ContentValues();

        values.put("NOM_JOUEUR", j.getNom());

        return bdd.insert("table_joueurs", null, values);
    }
    //Function : Modifie un objet joueur dans la base de données.
    //IN : Un objet de la classe joueur
    //OUT : le nombre d'enregistrmeent modifié
    public long modifJoueur(Joueur j) {
        ContentValues values = new ContentValues();

        values.put("NOM_JOUEUR", j.getNom());

        return bdd.update("table_joueurs", values, "id_joueur = ?", new String[] {String.valueOf(j.getId())});
    }

    //Function : Supprime un objet joueur dans la base de données.
    //IN : Un objet de la classe joueur
    //OUT : retourne le nombre d'objet joueur supprimé
    public long delJoueur(Joueur j) {
        return bdd.delete("table_joueurs", "id_joueur = ?", new String[]{String.valueOf(j.getId())});
    }

    //Function : Récupère un objet joueur dans la base de données.
    //IN : Un entier représentant le numéro identifiant d'un joueur
    //OUT : un objet Joueur
    public Joueur getJoueur(int id) {
        Cursor c = bdd.rawQuery("SELECT * FROM table_joueurs where id_joueur = ?", new String[]{String.valueOf(id)});
        Joueur j = null;
        while (c.moveToNext()) {
            j = new Joueur(c.getString(1));
            j.setId(c.getInt(0));
        }
        return j;
    }

    //Function : Récupère un vecteur contenant tous les objets joueurs de la base de données
    //IN:
    //OUT: Un vecteur d'objet joueur
    public Vector<Joueur> getAllJoueur() {
        Vector<Joueur> vJoueur = new Vector<Joueur>();
        Cursor c = bdd.rawQuery("SELECT * FROM table_joueurs", null);
        while (c.moveToNext()) {
            Joueur j = new Joueur(c.getString(1));
            j.setId(c.getInt(0));
            vJoueur.add(j);
        }
        return vJoueur;
    }

    //-------------------FONCTIONS LIES A OBJET GRILLE -------------------------------------------------

    //Function : Insere un objet grille dans la base de données.
    //IN : Un objet de la classe grille
    //OUT : l'id du nouvel enregistrement
    public long insertNewGrille(Grille g) {
        ContentValues values = new ContentValues();

        values.put("ID_JOUEUR", g.getId_joueur());

        return bdd.insert("table_grille", null, values);
    }

    //Function : Récupère un objet grille dans la base de données.
    //IN : Un entier représentant le numéro identifiant de la grille
    //OUT : UN objet de type Grille
    public Grille getGrille(int id) {
        Cursor c = bdd.rawQuery("SELECT * FROM table_grille where id_grille = ?", new String[]{String.valueOf(id)});
        Grille g = null;
        while (c.moveToNext()) {
            g = new Grille(c.getInt(c.getColumnIndexOrThrow("ID_JOUEUR")));
            g.setId(c.getInt(0));
            g.setScore_un(c.getInt(1));
            g.setScore_deux(c.getInt(2));
            g.setScore_trois(c.getInt(3));
            g.setScore_quatre(c.getInt(4));
            g.setScore_cinq(c.getInt(5));
            g.setScore_six(c.getInt(6));
            g.setScore_brelan(c.getInt(7));
            g.setScore_carre(c.getInt(8));
            g.setScore_chance(c.getInt(9));
            //Convertir int to boolean
            g.setScore_full(c.getInt(10));
            g.setScore_petite(c.getInt(11));
            g.setScore_grande(c.getInt(12));
            g.setScore_yams(c.getInt(13));
            g.setScore_totalHaut(c.getInt(14));
            g.setScore_totalBas(c.getInt(15));
        }
        return g;
    }
    //Function : Recupere toutes les grilles
    //IN:
    //OUT: un vecteur d'objet grille
    public Vector<Grille> getAllGrille() {
        Vector<Grille> vGrille = new Vector<Grille>();
        Cursor c = bdd.rawQuery("SELECT * FROM table_grille", null);
        while (c.moveToNext()) {
            Grille g = new Grille(c.getInt(c.getColumnIndexOrThrow("ID_JOUEUR")));
            g.setId(c.getInt(0));
            g.setScore_un(c.getInt(1));
            g.setScore_deux(c.getInt(2));
            g.setScore_trois(c.getInt(3));
            g.setScore_quatre(c.getInt(4));
            g.setScore_cinq(c.getInt(5));
            g.setScore_six(c.getInt(6));
            g.setScore_brelan(c.getInt(7));
            g.setScore_carre(c.getInt(8));
            g.setScore_chance(c.getInt(9));
            g.setScore_full(c.getInt(10));
            g.setScore_petite(c.getInt(11));
            g.setScore_grande(c.getInt(12));
            g.setScore_yams(c.getInt(13));
            g.setScore_totalHaut(c.getInt(14));
            g.setScore_totalBas(c.getInt(15));
            vGrille.add(g);
        }
        return vGrille;
    }
    //Function : Recupere toutes les grilles d'un joueur
    //IN:
    //OUT: un vecteur d'objet grille
    public Vector<Grille> getAllGrilleJoueur(Joueur j) {
        Vector<Grille> vGrille = new Vector<Grille>();
        Cursor c = bdd.rawQuery("SELECT * FROM table_grille WHERE id_joueur = ?", new String[] {String.valueOf(j.getId())});
        while (c.moveToNext()) {
            Grille g = new Grille(c.getInt(c.getColumnIndexOrThrow("ID_JOUEUR")));
            g.setId(c.getInt(0));
            g.setScore_un(c.getInt(1));
            g.setScore_deux(c.getInt(2));
            g.setScore_trois(c.getInt(3));
            g.setScore_quatre(c.getInt(4));
            g.setScore_cinq(c.getInt(5));
            g.setScore_six(c.getInt(6));
            g.setScore_brelan(c.getInt(7));
            g.setScore_carre(c.getInt(8));
            g.setScore_chance(c.getInt(9));
            g.setScore_full(c.getInt(10));
            g.setScore_petite(c.getInt(11));
            g.setScore_grande(c.getInt(12));
            g.setScore_yams(c.getInt(13));
            g.setScore_totalHaut(c.getInt(14));
            g.setScore_totalBas(c.getInt(15));
            vGrille.add(g);
        }
        return vGrille;
    }
    //Function: Met à jour une grille
    //IN: un objet Grille
    //OUT: le nombre de grille mise à jour
    public long updateGrille(Grille g){
        ContentValues values = new ContentValues();
        values.put("UN",g.getScore_un());
        values.put("DEUX",g.getScore_deux());
        values.put("TROIS",g.getScore_trois());
        values.put("QUATRE",g.getScore_quatre());
        values.put("CINQ",g.getScore_cinq());
        values.put("SIX",g.getScore_six());
        values.put("BRELAN",g.getScore_brelan());
        values.put("CARRE",g.getScore_carre());
        values.put("CHANCE",g.getScore_chance());
        values.put("FULL",g.getScore_full());
        values.put("PETITE",g.getScore_petite());
        values.put("GRANDE",g.getScore_grande());
        values.put("YAMS",g.getScore_yams());
        values.put("TOTAL_HAUT", g.getScore_totalHaut());
        values.put("TOTAL_BAS", g.getScore_totalBas());

        return bdd.update("table_grille",values, "id_grille = ?", new String[] {String.valueOf(g.getId())});
    }

    public void delGrille(){
        bdd.delete("table_lien_partie_grille","1=1",null);
        bdd.delete("table_grille","1=1", null);
        bdd.delete("table_partie","1=1",null);
    }

    //----------------------------Fonction liées à l'objet partie--------------------------------------

    //Function : Insere un objet partie dans la base de données.
    //IN : Un objet de la classe partie
    //OUT : l'id du nouvel enregistrement
    public long insertNewPartie(Partie p) {
        ContentValues values = new ContentValues();
        values.putNull("ID_PARTIE");
        return bdd.insert("table_partie", null, values);
    }

    //Function : Insere un nouvel enregistrement dans la table lien_partie_grille
    //IN:
    //OUT:
    public void insertGrilleIntoPartie(Partie p){
        for(Grille g:p.getvGrille()){
            Log.d("POUSS","partie : " +String.valueOf(p.getId()));
            Log.d("POUSS","Grille : " +String.valueOf(g.getId()));
            ContentValues values = new ContentValues();
            values.put("ID_PARTIE",p.getId());
            values.put("ID_GRILLE",g.getId());
            bdd.insert("table_lien_partie_grille",null,values);
        }
    }

    //Function: Récupère un enregistrement de la table partie.
    //IN: un entier représentant l'identifiant de la partie
    //OUT:Un objet Partie
    public Partie getPartie(int id){
        Partie p = new Partie();
        Vector<Grille> vG = new Vector<Grille>();
        Cursor c = bdd.rawQuery("SELECT * FROM table_partie where id_partie = ?", new String[]{String.valueOf(id)});
        Cursor cGrille = bdd.rawQuery("SELECT * FROM table_lien_partie_grille where id_partie = ?", new String[] {String.valueOf(id)});
        while(c.moveToNext()){
           // p = new Partie();
            p.setId(c.getInt(0));
            while(cGrille.moveToNext()) {
                vG.add(getGrille(cGrille.getInt(1)));
            }
            p.setvGrille(vG);
        }
        return p;
    }

    //Function: Récupère un enregistrement de la table partie.
    //IN: un entier représentant l'identifiant de la partie
    //OUT:Un objet Partie
    public int getIdPartie(Grille g){
        Cursor cGrille = bdd.rawQuery("SELECT id_partie FROM table_lien_partie_grille where id_grille = ?", new String[] {String.valueOf(g.getId())});
           cGrille.moveToNext();
        return cGrille.getInt(0);
    }
}
