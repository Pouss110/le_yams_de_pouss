package fr.devmonalex.leyamsdepouss.modele;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by dl211 on 05/10/2015.
 */
public class BddSQLite extends SQLiteOpenHelper {

    //table joueur
    private static final String TABLE_JOUEURS = "table_joueurs";
    private static final String COL_ID_JOUEUR = "ID_JOUEUR";
    private static final String COL_NOM_JOUEUR = "NOM_JOUEUR";

    private static final String CREATE_TABLE_JOUEUR = "CREATE TABLE " + TABLE_JOUEURS + " ("
            + COL_ID_JOUEUR + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COL_NOM_JOUEUR + " TEXT NOT NULL);";

    //table grille
    private static final String TABLE_GRILLE = "table_grille";
    private static final String COL_ID_GRILLE = "ID_GRILLE";
    private static final String COL_UN = "UN";
    private static final String COL_DEUX = "DEUX";
    private static final String COL_TROIS = "TROIS";
    private static final String COL_QUATRE = "QUATRE";
    private static final String COL_CINQ = "CINQ";
    private static final String COL_SIX = "SIX";
    private static final String COL_BRELAN = "BRELAN";
    private static final String COL_CARRE = "CARRE";
    private static final String COL_CHANCE = "CHANCE";
    private static final String COL_FULL = "FULL";
    private static final String COL_PETITE = "PETITE";
    private static final String COL_GRANDE = "GRANDE";
    private static final String COL_YAMS = "YAMS";
    private static final String COL_TOTALHAUT = "TOTAL_HAUT";
    private static final String COL_TOTALBAS = "TOTAL_BAS";

    private static final String CREATE_TABLE_GRILLE = "CREATE TABLE " + TABLE_GRILLE + " ("
            + COL_ID_GRILLE + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COL_UN + " TINYINT UNSIGNED, " + COL_DEUX + " TINYINT UNSIGNED, " + COL_TROIS + " TINYINT UNSIGNED, "
            + COL_QUATRE + " TINYINT UNSIGNED, " + COL_CINQ + " TINYINT UNSIGNED, " + COL_SIX + " TINYINT UNSIGNED, "
            + COL_BRELAN + " TINYINT UNSIGNED, " + COL_CARRE + " TINYINT UNSIGNED, " + COL_CHANCE + " TINYINT UNSIGNED, "
            + COL_FULL + " TINYINT UNSIGNED, " + COL_PETITE + " TINYINT UNSIGNED, " + COL_GRANDE + " TINYINT UNSIGNED, " + COL_YAMS + " TINYINT UNSIGNED, "
            + COL_TOTALHAUT + " TINYINT UNSIGNED, " + COL_TOTALBAS + " TINYINT UNSIGNED, " + COL_ID_JOUEUR + " INTEGER NOT NULL, "
            + " FOREIGN KEY ("+COL_ID_JOUEUR+") REFERENCES " +TABLE_JOUEURS +"(" + COL_ID_JOUEUR + "));";

    //table partie
    private static final String TABLE_PARTIE = "table_partie";
    private static final String COL_ID_PARTIE = "ID_PARTIE";
    private static final String COL_DATE_PARTIE = "DATE_PARTIE";

    private static final String CREATE_TABLE_PARTIE = "CREATE TABLE " + TABLE_PARTIE + " ("
            + COL_ID_PARTIE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_DATE_PARTIE + " DATE DEFAULT CURRENT_TIMESTAMP);";

    //table lien partie grille
    private static final String TABLE_LIEN_PARTIE_GRILLE = "table_lien_partie_grille";
    private static final String CREATE_TABLE_LIEN_PARTIE_GRILLE = "CREATE TABLE " + TABLE_LIEN_PARTIE_GRILLE + " ("
            + COL_ID_PARTIE + " INTEGER NOT NULL, " + COL_ID_GRILLE + " INTEGER NOT NULL, "
            + " FOREIGN KEY ("+COL_ID_PARTIE+") REFERENCES "+TABLE_PARTIE+"("+COL_ID_PARTIE+"),"
            + " FOREIGN KEY ("+COL_ID_GRILLE+") REFERENCES "+TABLE_GRILLE+"("+COL_ID_GRILLE+"));";


    public BddSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_JOUEUR);
        db.execSQL(CREATE_TABLE_GRILLE);
        db.execSQL(CREATE_TABLE_PARTIE);
        db.execSQL(CREATE_TABLE_LIEN_PARTIE_GRILLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + TABLE_LIEN_PARTIE_GRILLE +";");
        db.execSQL("DROP TABLE " + TABLE_PARTIE +";");
        db.execSQL("DROP TABLE " + TABLE_GRILLE +";");
        db.execSQL("DROP TABLE " + TABLE_JOUEURS +";");
        onCreate(db);
    }
}
