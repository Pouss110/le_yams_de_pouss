package fr.devmonalex.leyamsdepouss.modele;

import java.util.Vector;

/**
 * Created by dl211 on 08/10/2015.
 */
public class Partie {
    private String date;
    private int id;
    private Vector<Grille> vGrille;

    public Partie() {
       this.vGrille = new Vector<Grille>();
    }

    public Vector<Grille> getvGrille() {
        return vGrille;
    }

    public void setvGrille(Vector<Grille> vGrille) {
        this.vGrille = vGrille;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
