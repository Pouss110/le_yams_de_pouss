package fr.devmonalex.leyamsdepouss.modele;

import java.util.HashMap;

/**
 * Created by dl211 on 05/10/2015.
 *
 * Classe représentant un objet de la table joueur de la bdd
 *
 *
 */
public class Joueur {

    private int id;
    private String nom;

    public Joueur(String nom){
        this.setNom(nom);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Joueur{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }
//Retourne les données membres d’un objet joueur sous forme d’un tableau associatif.
    public HashMap<String, String> toHash(){
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("nom_joueur",this.getNom());
        map.put("id_joueur", String.valueOf(this.getId()));
        return map;
    }
}
